#!/usr/bin/env bash

source $(dirname $0)/common.sh

function test_update_readme() {
    local content="$1"

    echo "$content" > README.txt
    git add README.txt
    git commit -m "README $content"
}

function test_repository_mockup() {
    (
	cd $TMP_DIR
	mkdir mockup
	cd mockup
	git init -b main .
	git config user.email someone@example.com
	git config user.name 'Some One'
	test_update_readme main

	for stable in $STABLE ; do
	    git checkout -b release/$stable main
	    test_update_readme release/$stable
	done

	for b in $SOFTWARE $FEATURES ; do
	    git checkout -b $b main
	    test_update_readme $b
	done
	
	for stable in $STABLE ; do
	    for b in $SOFTWARE $STABLE_FEATURES ; do
		git checkout -b $stable/$b release/$stable
		test_update_readme $stable/$b
	    done
	done
    )
}

function test_repository_create() {
    export NAME=$1
    export REPO=$OWNER/$NAME
    
    (
	cd $TMP_DIR
	rsync -a --delete ./mockup/ ./$NAME/
	cd $NAME
	git checkout main
	git remote add $REMOTE $FORGEJO_SCHEME://$OWNER:$FORGEJO_TOKEN@$FORGEJO_INSTANCE/$OWNER/$NAME
	git push -u  $REMOTE main
	git push --all 
    )
}

function test_activate_trace() {
    set -x
    PS4='${BASH_SOURCE[0]}:$LINENO: ${FUNCNAME[0]}:  '
}

function test_deactivate_trace() {
    unset PS4
    set +x
}

function test_setup() {
    install_packages
    test_repository_mockup
    test_activate_trace
}
